[![Build
status](https://ci.appveyor.com/api/projects/status/jwpyqyne33eygu9r?svg=true)](https://ci.appveyor.com/project/eribul/incavis)
[![CRAN\_Status\_Badge](http://www.r-pkg.org/badges/version/incavis)](https://cran.r-project.org/package=incavis/)
[![Project Status: Active - The project has reached a stable, usable
state and is being actively
developed.](http://www.repostatus.org/badges/latest/active.svg)](http://www.repostatus.org/#active)

incavis
=======

`incavis` användas ihop med INCA:s funktionalitet för Vården i Siffror
(VIS) och "Kvalitetsindikatorer".

Installation
============

    # install.packages("devtools")
    devtools::install_bitbucket("cancercentrum/incavis")

Mer information
===============

Dokumentation för RCC:s arbete med Vården i Siffror utgår från [denna
wiki](https://bitbucket.org/cancercentrum/incavis/wiki).

Paketet dokumenteras ovan iform av vignettes med exempel (kan också
öppnas innifrån paketet efter installation på lokal dator), samt genom
dokumentation för respektive funktion i paketet (se "Reference" ovan
respektive `?fun` i R).

Paketversion
============

Det rekommenderas att använda paketet med samma version lokalt och i
INCA:s produktionsmiljö. Observera att den installerade versionen i INCA
kan skilja sig från den senaste utvecklingsversionen som presenteras
här. Jämför paketets interna dokumentation (`help(, "incavis")`) på INCA
för eventuell jämförelse och felsökning.

Paketets syfta
--------------

Paketets syfte är tredelat:

-   Att förbereda data och dess variabler till det format INCA och VIS
    kräver enligt upprättat tjänstekontrakt mellan Iera och RCC.
-   Att validera att data är korrekt och följer den angivna strukturen.
-   Att slutligen exportera data till en csv2-fil (namngiven
    `output.txt`) som sedan hanteras vidare av INCA.
